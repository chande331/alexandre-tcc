import React, { Component } from "react";
import {
    Card, CardImg, CardText, CardBody,
    CardTitle, CardSubtitle, Button
} from 'reactstrap';

export default class Page extends Component {

    constructor(props) {
        super(props);

        this.state = {
            corrente: [],
            valid: true,
        };
        this.pegaCorrente = this.pegaCorrente.bind(this)

    }

    componentDidMount() {
        this.pegaCorrente()
    }

    async testarq() {
        let result = await fetch('/api/criaarq')
       
    }

//valida a corrente
    async valida(){
        let result = await fetch('/api/verificachain')
        let result2 = await result.json()
        this.setState({
            valid: result2
        })
    }

    //pega a corrente
    async pegaCorrente() {
        let result = await fetch('/api/blockchain');
        console.log("result", result)
        let result2 = await result.json();

        console.log("resutlado boy", result2)
        let data = []
        for (let i in result2.cadeia) {
            data.push(result2.cadeia[i]);
        }
        console.log("wtf", data)
        this.setState({
            corrente: data,
        })
        return result
    }



    render() {
        return (
            <div>
                <div>
                <Button onClick={() => this.testarq()}> Gerar arquivo block </Button>
                </div>
                <div style={{marginTop: 10}}>
                    <Button onClick={()=>this.valida()}> Validar cadeia </Button>
                    <p>{this.state.valid ? 'Cadeia valida' : 'Cadeia invalida'}</p>
                </div>

                {this.state.corrente.map(m => {
                    return (
                        <div style={{ width: 200, height: 'auto', display: 'inline-block', backgroundColor: '#aaaaaa', wordWrap: 'break-word', margin: 10 }}>
                            <Card body outline color="secondary"    >
                                <CardBody>
                                    <CardTitle>Bloco {m.index}</CardTitle>
                                    <CardSubtitle>Dados do bloco</CardSubtitle>
                                    <CardText>
                                        <p><b>Hash:</b> {m.hash}</p>
                                        <p><b>Hash anterior:</b> {m.hashAnterior}</p>
                                        <p><b>Enviante:</b> {m.data.chaveEnviante}</p>
                                        <p><b>Receptor:</b> {m.data.chaveReceptor}</p>
                                        <p><b>Ativo:</b> {m.data.ativo}</p>
                                        <p><b>Valor:</b> {m.data.valor}</p>

                                    </CardText>
                                </CardBody>
                            </Card>
                        </div>

                    )
                })}

            </div>
        )

    }

}
