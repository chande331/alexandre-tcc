import React, { Component } from 'react';
import { Navbar, Nav, NavItem, MenuItem } from 'react-bootstrap';
import './App.css';
import { BrowserRouter as Router, Route, Link, Switch } from "react-router-dom";
import Carteira from './carteira.js';
import Page from './pagebloco.js'
import logo from './logo.svg';
class App extends Component {
  state = {
    response: ''
  };

  componentDidMount() {
    //this.callApi()
      //.then(res => this.setState({ response: res.express }))
      //.catch(err => console.log(err));
  }

  callApi = async () => {
    const response = await fetch('/api/mensagem');
    const body = await response.json();
    if (response.status !== 200) throw Error(body.message);

    return body;
  };

  addbloco = async () => {
    const response = await fetch('/api/mensagem2');
    const body = await response.json();
    if (response.status !== 200) throw Error(body.message);

    return body;
  }
  render() {
    return (
      <Router>

        <div className="App">

          <Navbar>
            <Navbar.Header>
              <Navbar.Brand>
                <Link to="/">Blockchain</Link>
              </Navbar.Brand>
            </Navbar.Header>
            <Nav>
              <NavItem eventKey={1} href="/page" >
                Blockchain
             </NavItem>
              <NavItem eventKey={2} href="/carteira">
                Carteira
            </NavItem>
            </Nav>
          </Navbar>
          <Route path="/page" component={Page} />
          <Route path="/carteira" component={Carteira} />
          <p className="App-intro">{this.state.response}</p>


        </div>
      </Router>
    );
  }
}

export default App;