import React, { Component } from "react";
import { FormGroup, Form, FormControl, Button } from 'react-bootstrap';
import Loader from 'react-loader'

export default class Carteira extends Component {

    constructor(props) {
        super(props);

        this.state = {
            id: '',
            idp: '',
            id2: '',
            saldo: 0,
            ativo: [],
            corrente: [],
            chave1: '',
            chaves: [],
            nomecarteira: '',
            carteiravalor: 0,
            carteiraativo: '',
            carteiravalor2: 0,
            carteiraativo2: '',
            carteiravalor3: 0,
            carteiraativo3: '',
            carregar: true,
        };
        this.consultaSaldo = this.consultaSaldo.bind(this)
        this.pegaCorrente = this.pegaCorrente.bind(this)
        this.enviaValorSmart = this.enviaValorSmart.bind(this)
        this.gerarChave = this.gerarChave.bind(this)
        this.pegarChaves = this.pegarChaves.bind(this)
        this.colocaChave = this.colocaChave.bind(this)
        this.colocaChave2 = this.colocaChave2.bind(this)
        this.retornapublic = this.retornapublic.bind(this)


    }

    componentDidMount() {
        this.pegaCorrente()
        this.pegarChaves()
    }

    colocaChave(myid) {
        let mypvt
        this.state.chaves.map(c => {
            if (myid === c.private) {
                return (mypvt = c.public)
            }
        })
        this.setState({
            id: myid,
            idp: mypvt
        }, () => this.consultaSaldo())

    }

    colocaChave2(myid) {
        this.setState({
            id2: myid
        })
    }
    //gera chave publica e privada
    async gerarChave(myname) {
        let result = await fetch('/api/gerarchave', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                nome: myname,
            })
        })
        let result2 = await result.json()
        console.log("minha chave", result2)
        this.setState({
            id: result2.private,
            carregar: true
        }, () => this.pegarChaves())

    }
    //teste da chave
    testaChave() {
        this.state.chaves.map(c => {
            if (this.state.chave1 === c.private) {
                console.log("chave publica", c.public)
            }
        })
    }
    //retorna a cahve publica
    retornapublic(chaveP) {

        return (
            this.state.chaves.map(m => {
                if (chaveP === m.public) {
                    return m.private
                }
            })
        )
    }


    //pega as chaves
    async pegarChaves() {
        let result = await fetch('/api/todasChaves')
        let result2 = await result.json()
        console.log("todas chaves", result2)
        let data = []
        for (let i in result2.key) {
            data.push(result2.key[i]);
        }
        console.log("wtf", data)
        this.setState({
            chaves: data,
        })
        return result2
    }
    //pega a blockchain
    async pegaCorrente() {
        let result = await fetch('/api/blockchain');
        console.log("result", result)
        let result2 = await result.json();

        console.log("resutlado boy", result2)
        let data = []
        for (let i in result2.cadeia) {
            data.push(result2.cadeia[i]);
        }
        console.log("wtf", data)
        this.setState({
            corrente: data,
        })
        return result
    }
    //envia o ativo no smart contrat
    async enviaAtivoSmart(valor, ativo, chaveEnviante, chaveReceptor) {
        console.log("enviando")
        let result = await fetch('/api/enviaSmartAtivo', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                valor: valor,
                ativo: ativo,
                chaveEnviante: chaveEnviante,
                chaveReceptor: chaveReceptor,
                cond: true,
                tipo: true
            })
        })
        this.pegaCorrente()

        return result
    }
    //envia o valor no smart contrat
    async enviaValorSmart(valor, ativo, chaveEnviante, chaveReceptor) {
        console.log("enviando")
        let result = await fetch('/api/enviaSmartValor', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                valor: valor,
                ativo: ativo,
                chaveEnviante: chaveEnviante,
                chaveReceptor: chaveReceptor,
                cond: true,
                tipo: true
            })
        })
        this.pegaCorrente()

        return result
    }
    //envia um valor na blockchain
    async enviaValor(valor, chaveEnviante, chaveReceptor) {
        console.log("enviando")
        let result = await fetch('/api/enviaValor', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                valor: valor,
                ativo: '',
                chaveEnviante: chaveEnviante,
                chaveReceptor: chaveReceptor,
                cond: true,
                tipo: true
            })
        })
        this.pegaCorrente()

        return result
    }

    //consulta o saldo
    consultaSaldo() {
        let pos = 0
        let saldo = 0
        let ativo = []
        let achou = false;
        let dados = []
        dados = this.state.corrente
        var posf;
        let soma = 0;
        console.log("dado saldo", dados)
        console.log("wtf mei mein", this.state.idp)
        dados.map(atual => {
            if (atual.index !== 0) {
                if (atual.data.chaveEnviante === this.state.idp) {
                    saldo = saldo - atual.data.valor
                    console.log("testando o atual", atual)
                    if (atual.data.ativo !== "") {
                        ativo.push(atual.data.ativo)
                    }
                }
                if (atual.data.chaveReceptor === this.state.idp) {
                    saldo = parseInt(saldo, 10) + parseInt(atual.data.valor, 10);
                    if (atual.data.ativo !== "") {
                        soma = 0;
                        if (ativo.length > 0) {
                            ativo.map(ativoAT => {
                                if (ativoAT === atual.data.ativo) {
                                    posf = soma;
                                    achou = true
                                }
                                soma = soma + 1
                            })
                        }
                    }
                    if (achou === true) {
                        console.log("soma", soma)
                        console.log("entrei aqui fio", ativo)
                        ativo.splice(soma - 1);
                        console.log("saindo aqui", ativo)
                    }
                }
            }

        })

        this.setState({
            ativo: ativo,
            saldo: saldo
        })

        //retornar o valor do saldo e o valor dos ativos 



    }

    render() {
        return (
            <div>
                <div>
                    <b>Carteira </b>{this.state.chaves.map(c => {
                        if (c.private === this.state.id)
                            return (
                                c.nome
                            )
                    })}
                    <div>
                        <div>Saldo: {this.state.saldo} </div>
                        <div>Ativo: {this.state.ativo} </div>
                    </div>
                </div>
                <div style={{ borderWidth: 1, borderStyle: 'solid', width: 300 }}>
                    Escolha sua carteira
                    {this.state.chaves.map(c => {
                        return (
                            <div style={{ backgroundColor: (c.private === this.state.id) ? '#aaaaaa' : '#ffffff' }} onClick={() => this.colocaChave(c.private)}>
                                <p>{c.nome}</p>
                            </div>
                        )
                    })}
                </div>
                <Loader loaded={this.state.carregar}>
                    <div>
                        <div> Criar carteira </div>
                        <Form>
                            <FormGroup
                                type="text"
                                label="Text"
                                placeholder="Coloque o nome"
                            />
                            <FormControl type={'text'} onChange={(e) => { this.setState({ nomecarteira: e.target.value }) }} value={this.state.nomecarteira} />
                            <FormGroup />
                        </Form>
                        <Button onClick={() => { this.gerarChave(this.state.nomecarteira); this.setState({ carregar: false }) }}>Criar carteira </Button>
                    </div>
                </Loader>

                <div style={{ display: 'inline-block', paddingTop: 15 }}>
                    <div>
                        <div style={{ borderWidth: 1, borderStyle: 'solid', width: 300, display: 'inline-block', margin: 5 }}>
                            Escolha para quem enviar
                            {this.state.chaves.map(c => {
                                return (

                                    <div style={{ backgroundColor: (c.public === this.state.id2) ? '#aaaaaa' : '#ffffff' }} onClick={() => this.colocaChave2(c.public)}>
                                        <p>{c.nome}</p>
                                    </div>
                                )
                            })}
                        </div>
                        <div style={{ borderWidth: 2, borderStyle: 'solid', width: 300, display: 'inline-block', margin: 5 }}>
                            <div> ENVIAR ATIVO CONTRATO </div>
                            <Form>
                                <div>Valor a ser pago: </div>
                                <FormGroup
                                    type="text"
                                    label="Text"
                                    placeholder="Coloque o valor"
                                />
                                <FormControl type={'text'} onChange={(e) => { this.setState({ carteiravalor: e.target.value }) }} value={this.state.carteiravalor} />
                                <FormGroup />
                                <div>Ativo a ser enviado: </div>
                                <FormGroup
                                    type="text"
                                    label="Text"
                                    placeholder="Coloque o valor"
                                />
                                <FormControl type={'text'} onChange={(e) => { this.setState({ carteiraativo: e.target.value }) }} value={this.state.carteiraativo} />
                                <FormGroup />
                            </Form>
                            <Button onClick={() => this.enviaAtivoSmart(this.state.carteiravalor, this.state.carteiraativo, this.state.id, this.state.id2)}>Enviar ativo </Button>
                        </div>
                        <div style={{ borderWidth: 2, borderStyle: 'solid', width: 300, display: 'inline-block', margin: 5 }}>
                            <div> ENVIAR VALOR CONTRATO </div>
                            <Form>
                                <div>Valor a ser enviado: </div>
                                <FormGroup
                                    type="text"
                                    label="Text"
                                    placeholder="Coloque o valor"
                                />
                                <FormControl type={'text'} onChange={(e) => { this.setState({ carteiravalor2: e.target.value }) }} value={this.state.carteiravalor2} />
                                <FormGroup />
                                <div>Ativo a ser recebido: </div>
                                <FormGroup
                                    type="text"
                                    label="Text"
                                    placeholder="Coloque o valor"
                                />
                                <FormControl type={'text'} onChange={(e) => { this.setState({ carteiraativo2: e.target.value }) }} value={this.state.carteiraativo2} />
                                <FormGroup />
                            </Form>
                            <Button onClick={() => this.enviaValorSmart(this.state.carteiravalor2, this.state.carteiraativo2, this.state.id, this.state.id2)}>Enviar valor </Button>
                        </div>
                        <div style={{ borderWidth: 2, borderStyle: 'solid', width: 300, display: 'inline-block', margin: 5 }}>
                            <div> ENVIAR VALOR DIRETO </div>
                            <Form>
                                <div>Valor a ser enviado: </div>
                                <FormGroup
                                    type="text"
                                    label="Text"
                                    placeholder="Coloque o valor"
                                />
                                <FormControl type={'text'} onChange={(e) => { this.setState({ carteiravalor3: e.target.value }) }} value={this.state.carteiravalor3} />
                                <FormGroup />
                            </Form>
                            <Button onClick={() => this.enviaValor(this.state.carteiravalor3, this.state.id, this.state.id2)}>Enviar valor </Button>
                        </div>
                    </div>
                </div>

            </div>
        )

    }

}
