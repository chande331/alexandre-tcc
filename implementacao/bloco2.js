const transacao = require('./transacao2')
var md5 = require('md5');

class Bloco {
    constructor(inx, t, HashAnt){
        this.index = inx;
        this.dado = t;
        this.HashAnterior = HashAnt;
        this.MeuHash = GerarHash();
    }  


//Pegar o index do bloco

PegarIndex() {
	return this.index;
}

GerarHash() {
	let criaHash = this.dado.valor.toString() + this.dado.chaveEnviante + this.dado.chaveReceptor + this.dado.timestamp.toString() + this.dado.ativo;
	//hash<string> tipoHash; //transformar os dados da string em hash
	//hash<string> antHash; //pegar a hash anterior
	return md5(criaHash + HashAnterior);

}

PegarHash() {
	return this.MeuHash;
}

PegarHashAnterior() {
	return this.HashAnterior;
}

validaHash() {
	return GerarHash() === PegarHash();
}


}

module.exports = Bloco;