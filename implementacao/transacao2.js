class Transacao {
    constructor(val, ati, KE, KR, tempo, condicao, type){
        this.valor = val; // float 
		this.ativo = ati; //string
		this.chaveEnviante = KE; //string - chave
		this.chaveReceptor = KR; //string - chave
		this.timestamp = tempo; //tempo - int
		this.cond = condicao; //booleano
		this.tipo = type; //booleano
    }  

}

module.exports = Transacao;