const Block = require('./bloco')

class Blockchain {
    constructor(difficulty = 1) {
        this.blocos = [new Block()]
        this.index = 1
        this.difficulty = difficulty
        this.smart = [];
    }

    //retorna o ultimo bloco
    getLastBlock() {
        return this.blocos[this.blocos.length - 1]
    }
    //adiciona um bloco
    addBlock(data) {
        const index = this.index
        const difficulty = this.difficulty
        const hashAnterior = this.getLastBlock().hash
        let block
        let pos = 0;
        let achou = false;
        if(data.cond === false){
             block = new Block(index, hashAnterior, data, difficulty)
             this.index++
             this.blocos.push(block)
        }else{
            console.log("entrei aqui, sou smart")
            for(let i =0; i < this.smart.length ; i++){
                if(this.smart[i].chaveEnviante === data.chaveReceptor && this.smart[i].chaveReceptor === data.chaveEnviante && this.smart[i].valor === data.valor
                    && this.smart[i].ativo === data.ativo){
                        achou = true;
                        if(this.smart[i].tipo === true){
                            block = new Block(index, hashAnterior, this.smart[i], difficulty)
                            this.index++
                            this.blocos.push(block)
                        }
                        if(data.tipo === true){
                            block = new Block(index, hashAnterior, data, difficulty)
                            this.index++
                            this.blocos.push(block)
                        }
                    }
                    pos = pos + 1;
            }
            if (achou === false) {
                this.smart.push(data);
            }
            else {
                this.smart.splice(pos - 1);
            }
    
        }

      
        
    }
    //mostra os blocos
    mostra(){
        for (let i = 0; i < this.blocos.length; i++){
            const currentBlock = this.blocos[i];
            console.log(currentBlock)
        }
    }

    cadeiaBlocos(){
        return this.blocos
    }

    //valida cadeia
    isValid() {
        for (let i = 1; i < this.blocos.length; i++) {
            const currentBlock = this.blocos[i]
            const previousBlock = this.blocos[i - 1]

            if (currentBlock.hash !== currentBlock.generateHash()) {
                return false
            }

            if (currentBlock.index !== previousBlock.index + 1) {
                return false
            }

            if (currentBlock.hashAnterior !== previousBlock.hash) {
                return false
            }
        }
        return true
    }
}

module.exports = Blockchain